This is the README for the Java GUI project. This program takes in a text document from the command line parsing the words and storing them in a hashmap returning certain data. The data consists of the total word count, different word count, and the number of occurences of each word. When displaying the total word count  the program also provides the 5 most popular and 5 least popular words. 

To run from the command line make sure to cd into 450Java/src. Then make sure the text file you want to use is also stored in the TextFiles folder at 450Java/TextFiles. Then you can type "java Main ../TextFiles/<filename>.txt " and press enter to run. Then open up the OutputFile.txt which in 450Java/src to see the data about your input file.


There's also a second version which has the functionalities listed above but the user needs to interact with the program through a graphical user interface.

To run the second version either open the 450Java project up in your IDE and run the GuiMain.java file which will then open up a GUI window. Then click the "Choose File" button which will open up the File Chooser. In the File Chooser click the TextFiles folder and choose a .txt file to run through the program, or go to wherever you stored a .txt file on your machine and open it up from there with the File Chooser.. The 5 most and least popular words, as well as the total word count and total different word count will show up in the GUI window. Then open up the GuiOutput.txt file in the 450Java folder to see the count of each word.

The second version can also be run from the command line by doing a cd into 450Java/src and typing "Java GuiMain". You follow the same steps as above from that point except the File chooser now opens up in the src folder so make sure to change the folder for the file chooser to 450Java for the provided documents or wherever your desired text document is.
