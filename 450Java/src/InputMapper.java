/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
import java.util.HashMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


/**
 * The InputMappper class exists to deal with the input file, The input file is
 * a text document and the class creates an InputMapper object, and then scans
 * the text file into a HashMap. It comes up with the total word count, count for
 * each word and five most and least popular words in the input file. 
 *
 * @author alimohamud
 */
public class InputMapper {

    protected HashMap<String, Integer> docMap;
    protected File file;
    protected int wordTotal = 0;

    /**
     * This constructor creates an InputMapper object. It first validates the file that's
     * being passed in checking that it does exist and that it is a .txt file.
     *
     * @param filename a .txt file.
     */
    public InputMapper(File filename) {
        if (filename.exists() && filename.getName().endsWith(".txt")) {
            file = filename;
        } else {

            System.out.println("Invalid file, make sure to use .txt file");
        }
        docMap = new HashMap<>();

    }

    /**
     *The fileMap() method takes an empty HashMap and populates it with words
     *from a text input file using a Scanner object. It takes each word in only
     *once and increases the value of the key for that word each time it comes
     * across the word again. 
     */
    private void fileMap() {

        try {
            Scanner scn = new Scanner(file);
            while (scn.hasNext()) {
                scn.useDelimiter("[^a-zA-Z']");
                String word = scn.next().toLowerCase();

                if (word.equals("")) { //skips over empty spaces
                    continue;
                }
                wordTotal++;
                if (docMap.containsKey(word)) {
                    int indivCount = docMap.get(word);
                    docMap.put(word, ++indivCount);
                } else {
                    docMap.put(word, 1);
                }

            }
            scn.close();

        } catch (FileNotFoundException ex) {
            System.err.println(ex);

        }

    }
    /**
     * This method returns the total number of words in the text input file by
     * running fileMap() which tracks the total number of words with the global
     * variable wordTotal.
     * @return an int representing how many words are in the input file.
     */
    protected int totalWords() {
        fileMap();
        return wordTotal;
    }
    
    /**
     * This converts the HashMap into an array, then sorts the objects 
     * from most to least based on the key's value.
     * @return an Object array containing the keys and values of the HashMap.
     */
    protected Object [] MapSort() {
        Object[] a = docMap.entrySet().toArray();

        Arrays.sort(a, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Map.Entry<String, Integer>) o2).getValue()
                        .compareTo(((Map.Entry<String, Integer>) o1).getValue());
            }
        });  
        //System.out.println(Arrays.toString(a));
        return a;
    }
    /**
     * This returns the total number of different words in the input file by
     * checking the length of MapSort()'s returned array which contains every
     * different word.
     * @return an int which is the number of different words
     */
    protected int countDiffWords(){
        int f = MapSort().length;
        return f;
    }
    /**
     * This method creates an ArrayList out of all the keys in the sorted array
     * returned from MapSort(). 
     * @return : an ArrayList containing all the keys from MapSort()
     */
    protected ArrayList<String> keyGetter(){
        ArrayList<String> pop = new ArrayList<String>();
        for (Object k: MapSort()){
            pop.add(((Map.Entry<String, Integer>) k).getKey());
            
        }
        
        return pop;
    }
    /**
     * This method creates a string of the five most popular words in the
     * input file. It does this by incrementing through the first five elements in
     * the sorted arrayList returned from keyGetter() and adds them to a new
     * arrayList which is returned through the toString() method.
     * @return A string of the five most popular words in the input file
     */
    protected String fiveMostPop(){
        ArrayList<String> pop = keyGetter();
        ArrayList<String> MostPop = new ArrayList<String>();
        for(int i = 0; i< 5; i++){
            MostPop.add(pop.get(i));
        }
        return MostPop.toString();
    }
    /**
     * This method creates a string of the five least popular words in the
     * input file. It does this by decrementing through the last five elements in
     * the sorted arrayList returned from keyGetter() and adds them to a new
     * arrayList which is returned through the toString() method.
     * @return A string of the five least popular words in the input file
     */
    protected String fiveLeastPop(){
        ArrayList<String> pop = keyGetter();
        ArrayList<String> MostPop = new ArrayList<String>();
        for (int j = pop.size()-1; j > pop.size()-5; j--){
            MostPop.add(pop.get(j));
        }
        return MostPop.toString();
    }
    /**
     * This method returns  the sorted array from MapSort() to show the count
     * of each word in order of frequency.
     * @return A string of the sorted array from MapSort()
     */
    protected String eachWordCount(){
        return Arrays.toString(MapSort());
    }
    
    
}
