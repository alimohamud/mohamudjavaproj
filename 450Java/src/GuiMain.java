
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/*
 */

/**
 *This is the main class for the GUI version of this application. It makes the
 * GuiWindow visible and triggers it to run. This can be run from here or
 * from the command line following the instructions outlined in the README.
 * @author alimohamud
 */
public class GuiMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        GuiWindow theWindow = new GuiWindow();
        theWindow.setVisible(true);
        
    }
    
}
