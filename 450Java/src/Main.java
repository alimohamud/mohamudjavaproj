
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 *This is the main class for the first version, to run it follow the instructions
 * outlined in the README for the first version. This is meant to run on the
 * command line.
 * @author alimohamud
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // Creates new empty file "OutputFile.txt" in 450Java folder
        if(args.length == 0){
            System.out.println("Usage: Main filename");
            System.exit(0);
        }
        else{
        BufferedWriter writer = new BufferedWriter(new FileWriter("OutputFile.txt")); 
        InputMapper run = new InputMapper(new File(args[0]));
        
        writer.write("The total number of words is: " + run.totalWords());
        writer.write(System.getProperty( "line.separator" ));
        writer.write(System.getProperty( "line.separator" ));
        writer.write("The total number of different words is: " + run.countDiffWords());
        writer.write(System.getProperty( "line.separator" ));
        writer.write(System.getProperty( "line.separator" ));
        writer.write("The count for each word is: ");
        writer.write(System.getProperty( "line.separator" ));
        writer.write(run.eachWordCount());
        writer.write(System.getProperty( "line.separator" ));
        writer.write(System.getProperty( "line.separator" ));
        writer.write("The five most popular words in this document are: ");
        writer.write(run.fiveMostPop());
        writer.write(System.getProperty( "line.separator" ));
        writer.write(System.getProperty( "line.separator" ));
        writer.write("The five least popular words in this document are: ");
        writer.write(run.fiveLeastPop());
        writer.close();
        }
        
    }

}
