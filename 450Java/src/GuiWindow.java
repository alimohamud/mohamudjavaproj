/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * This is the GUI Window class it controls all the visual elements of the
 * program. It also makes the appropriate calls to InputMapper to run the
 * programs. It does this by utilizing the Java Swing GUI library as well as the
 * Java AWT library for button functionality.
 *
 * @author Ali Mohamud
 */
public class GuiWindow extends JFrame implements ActionListener {

    private JButton button = new JButton("Choose a Text File");
    private File file;
    private JTextArea output = new JTextArea();

    /**
     * This is the constructor for the GuiWindow class. It builds the GUI window
     * and implements the text area and button.
     */
    public GuiWindow() {

        this.setTitle("Word Counter");
        this.setBounds(400, 400, 400, 300);
        this.getContentPane().setLayout(new FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.button.setBounds(50, 80, 100, 30);
        this.getContentPane().add(button);
        this.button.addActionListener(this);

        output.setEditable(false);
        this.add(output);
    }

    /**
     * This runs the File Chooser if the "Choose File" button was pressed and
     * then runs the GuiOutput method based on which file was chosen.
     *
     * @param e the Action Event of clicking the button
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Choose a Text File")) {
            this.fileChooser();
            try {
                this.GuiOutput(file);

            } catch (IOException ex) {
                Logger.getLogger(GuiWindow.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /**
     * This method creates the File Chooser from which the user can choose a
     * text file to run through the program. it directs the File Chooser to open
     * up in the 450Java folder although it lets the user open up a file stored
     * anywhere on their machine.
     */
    protected void fileChooser() {
        JFileChooser fc = new JFileChooser(".");
        fc.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        System.out.println("File Chooser created");
        int response = fc.showOpenDialog(null);
        System.out.println("File Chooser opened, it returned " + response);
        if (response == JFileChooser.APPROVE_OPTION) {
            file = fc.getSelectedFile();
            System.out.println("You chose the file located at " + file.getAbsolutePath());
            System.out.println("You chose the file " + file.getName());
        }

    }

    /**
     * This method creates an InputMapper object and then appends the data
     * collected onto the global text area "output". It also creates a file to
     * store the data of the count of each individual word.
     *
     * @param f the file being input
     * @throws FileNotFoundException
     * @throws IOException
     */
    protected void GuiOutput(File f) throws FileNotFoundException, IOException {
        if (f.isFile() && f.getName().endsWith(".txt")) {
            InputMapper run = new InputMapper(f);
            String str = "";
            //str += f.getAbsolutePath() + "\n";
            str += "The total number of words is: " + run.totalWords() + "\n" + "\n";
            str += "The total number of different words is: " + run.countDiffWords() + "\n" + "\n";
            str += "The five most popular words in this document are: " + "\n";
            str += run.fiveMostPop() + "\n";
            str += "\n";
            str += "The five least popular words in this document are: " + "\n";
            str += run.fiveLeastPop() + "\n" + "\n";
            output.append(str);

            BufferedWriter writer = new BufferedWriter(new FileWriter("GuiOutput.txt"));
            writer.write("The count for each word is: ");
            writer.write(System.getProperty("line.separator"));
            writer.write(run.eachWordCount());
            writer.close();

        }
    }

}
